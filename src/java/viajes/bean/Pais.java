/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viajes.bean;

import java.io.Serializable;

/**
 *
 * @author Jani
 */
public class Pais {

    private int codPais;
    private String descripcion;

    public int getCodPais() {
        return codPais;
    }

    public void setCodPais(int codPais) {
        this.codPais = codPais;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "codPais=" + codPais + ", descripcion=" + descripcion;
    }

}
