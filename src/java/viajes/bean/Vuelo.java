/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viajes.bean;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author Jani
 */

public class Vuelo {

    private int codVuelo;

    private String origen;
    private String destino;
    private String fecIda;
    private String horaSalida;
    private String fecVuelta;
    private String horaLlegada;

    public int getCodVuelo() {
        return codVuelo;
    }

    public void setCodVuelo(int codVuelo) {
        this.codVuelo = codVuelo;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getFecIda() {
        return fecIda;
    }

    public void setFecIda(String fecIda) {
        this.fecIda = fecIda;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getFecVuelta() {
        return fecVuelta;
    }

    public void setFecVuelta(String fecVuelta) {
        this.fecVuelta = fecVuelta;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

   

}
