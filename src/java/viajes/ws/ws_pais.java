/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viajes.ws;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import viajes.bean.Pais;
import viajes.util.MySqlDBConn;

/**
 *
 * @author Jani
 */
@WebService(serviceName = "ws_pais")
public class ws_pais {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "activo")
    public String activo() {
        return "Ok!";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "listPaises")
    public List<Pais> listPaises() {
        List<Pais> lstPais = new ArrayList<Pais>();

        try {
            MySqlDBConn mySql = new MySqlDBConn();
            Connection con = mySql.getConnection();
            Statement st = con.createStatement();
            String select = "select CodPais, Descripcion from pais";
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                Pais pais = new Pais();
                pais.setCodPais(rs.getInt(1));
                pais.setDescripcion(rs.getString(2));
                lstPais.add(pais);
            }

            st.close();
            con.close();

        } catch (Exception e) {
            //mensaje = "Error: " + e.getMessage();
            return null;
        }
        return lstPais;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "searchPais")
    public Pais searchPais(@WebParam(name = "codPais") Integer codPais) {
        Pais pais = new Pais();

        try {
            MySqlDBConn mySql = new MySqlDBConn();
            Connection con = mySql.getConnection();
            Statement st = con.createStatement();
            String select = "select CodPais, Descripcion from pais";
            String where = null;

            if (codPais != null) {
                where = " where codPais='" + codPais + "'";
            } else {
                where = "";
                return pais;
            }

            ResultSet rs = st.executeQuery(select + where);
            rs.next();

            pais.setCodPais(rs.getInt(1));
            pais.setDescripcion(rs.getString(2));

            st.close();
            con.close();

        } catch (Exception e) {
            //mensaje = "Error: " + e.getMessage();
            return null;
        }

        return pais;
    }

}
