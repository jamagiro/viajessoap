/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viajes.ws;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import viajes.bean.Ciudad;
import viajes.util.MySqlDBConn;

/**
 *
 * @author Jani
 */
@WebService(serviceName = "ws_ciudad")
public class ws_ciudad {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "activo")
    public String activo() {
        return "Ok!";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "listCiudades")
    public List<Ciudad> listCiudades() {
        List<Ciudad> lstCiudad = new ArrayList<Ciudad>();

        try {
            MySqlDBConn mySql = new MySqlDBConn();
            Connection con = mySql.getConnection();
            Statement st = con.createStatement();
            String select = "select CodCiudad, Descripcion, Pais_CodPais from ciudad";
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                Ciudad ciudad = new Ciudad();
                ciudad.setCodCiudad(rs.getInt(1));
                ciudad.setDescripcion(rs.getString(2));
                ciudad.setCodPais(rs.getInt(3));
                lstCiudad.add(ciudad);
            }

            st.close();
            con.close();

        } catch (Exception e) {
            //mensaje = "Error: " + e.getMessage();
            return null;
        }
        return lstCiudad;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "searchCiudad")
    public Ciudad searchCiudad(@WebParam(name = "codCiudad") Integer codCiudad) {
        Ciudad ciudad = new Ciudad();

        try {
            MySqlDBConn mySql = new MySqlDBConn();
            Connection con = mySql.getConnection();
            Statement st = con.createStatement();
            String select = "select CodCiudad, Descripcion, Pais_CodPais from ciudad";
            String where = null;

            if (codCiudad != null) {
                where = " where codCiudad='" + codCiudad + "'";
            } else {
                where = "";
                return ciudad;
            }

            ResultSet rs = st.executeQuery(select + where);
            rs.next();

            ciudad.setCodCiudad(rs.getInt(1));
            ciudad.setDescripcion(rs.getString(2));
            ciudad.setCodPais(rs.getInt(3));

            st.close();
            con.close();

        } catch (Exception e) {
            //mensaje = "Error: " + e.getMessage();
            return null;
        }

        return ciudad;
    }

}
