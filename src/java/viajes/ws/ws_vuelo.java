/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viajes.ws;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import viajes.bean.Vuelo;
import viajes.util.MySqlDBConn;

/**
 *
 * @author Jani
 */
@WebService(serviceName = "ws_vuelo")
public class ws_vuelo {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "activo")
    public String activo() {
        return "Ok!";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "listVuelos")
    public List<Vuelo> listVuelos() {
        List<Vuelo> lstVuelos = new ArrayList<Vuelo>();

        try {
            MySqlDBConn mySql = new MySqlDBConn();
            Connection con = mySql.getConnection();
            Statement st = con.createStatement();
            String select = "select CodVuelo, Origen, Destino, FecIda, HoraSalida, FecVuelta, HoraLlegada from vuelos";
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                Vuelo vuelo = new Vuelo();
                vuelo.setCodVuelo(rs.getInt(1));
                vuelo.setOrigen(rs.getString(2));
                vuelo.setDestino(rs.getString(3));
                vuelo.setFecIda(rs.getString(4));
                vuelo.setHoraSalida(rs.getString(5));
                vuelo.setFecVuelta(rs.getString(6));
                vuelo.setHoraLlegada(rs.getString(7));
                lstVuelos.add(vuelo);
            }

            st.close();
            con.close();

        } catch (Exception e) {
            //mensaje = "Error: " + e.getMessage();
            return null;
        }
        return lstVuelos;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "searchVuelo")
    public Vuelo searchVuelo(@WebParam(name = "codVuelo") Integer codVuelo) {
        Vuelo vuelo = new Vuelo();

        try {
            MySqlDBConn mySql = new MySqlDBConn();
            Connection con = mySql.getConnection();
            Statement st = con.createStatement();
            String select = "select CodVuelo, Origen, Destino, FecIda, HoraSalida, FecVuelta, HoraLlegada from vuelos";
            String where = null;

            if (codVuelo != null) {
                where = " where codVuelo='" + codVuelo + "'";
            } else {
                where = "";
                return vuelo;
            }

            ResultSet rs = st.executeQuery(select + where);
            rs.next();

            vuelo.setCodVuelo(rs.getInt(1));
            vuelo.setOrigen(rs.getString(2));
            vuelo.setDestino(rs.getString(3));
            vuelo.setFecIda(rs.getString(4));
            vuelo.setHoraSalida(rs.getString(5));
            vuelo.setFecVuelta(rs.getString(6));
            vuelo.setHoraLlegada(rs.getString(7));

            st.close();
            con.close();

        } catch (Exception e) {
            //mensaje = "Error: " + e.getMessage();
            return null;
        }

        return vuelo;
    }

}
